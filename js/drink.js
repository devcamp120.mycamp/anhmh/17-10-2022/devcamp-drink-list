//Tạo một class Drink
class Drink {
    //Hàm tạo Constructor
    constructor(paramId, paramMaNuocUong, paramTenNuocUong, paramDonGia, paramNgayTao, paramNgayCapNhat) {
        this.id = paramId;
        this.maNuocUong = paramMaNuocUong;
        this.tenNuocUong = paramTenNuocUong;
        this.donGia = paramDonGia;
        this.ngayTao = paramNgayTao;
        this.ngayCapNhat = paramNgayCapNhat;
    }
    //Phương thức của class Drink
    print() {
        return this.id + " " + this.maNuocUong + " " + this.tenNuocUong + " " + this.donGia + " " + this.ngayTao + " " + this.ngayCapNhat
    }
}
drinkListArray = [];
var traTac = new Drink(1,"TRATAC", "Trà tắc", 10000, "14/5/2021", "14/5/2021");
drinkListArray.push(traTac);
var coCa = new Drink(2,"COCA", "Cocacola", 15000, "14/5/2021", "14/5/2021");
drinkListArray.push(coCa); 
var pepSi = new Drink(1,"PEPSI", "Pepsi", 15000, "14/5/2021", "14/5/2021");
drinkListArray.push(pepSi);

var drinkListObject = [
    {
        "id": 1,
        "maNuocUong": "TRATAC",
        "tenNuocUong": "Trà tắc",
        "donGia": 10000,
        "ngayTao": "14/5/2021",
        "ngayCapNhat": "14/5/2021"
    },
    {
        "id": 2,
        "maNuocUong": "COCA",
        "tenNuocUong": "Cocacola",
        "donGia": 15000,
        "ngayTao": "14/5/2021",
        "ngayCapNhat": "14/5/2021"
    },
    {
        "id": 1,
        "maNuocUong": "PEPSI",
        "tenNuocUong": "Pepsi",
        "donGia": 15000,
        "ngayTao": "14/5/2021",
        "ngayCapNhat": "14/5/2021"
    }
];

module.exports = { drinkListArray, drinkListObject }